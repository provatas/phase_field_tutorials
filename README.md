# README #

This repository is where knowledge and how-to about phase field modeling are stored in a master files arranged by theme. Please title the file thematically therefore, and make contributions to each file with reasonably explanatory sections.


For more information contact Nikolas Provatas at provatas@physics.mcgill.ca