\documentclass[aps,preprint,preprintnumbers,amsmath,amssymb,floatfix]{revtex4}

% For inserting eps graphics files
\usepackage[final,dvips]{graphicx}
\usepackage{verbatim}
\usepackage{color}


\newcommand \rt {\right}
\newcommand \lt {\left}
\newcommand \nline {\nonumber \\}
\newcommand{\noid}{\noindent}
\newcommand \pxpy[2] {\frac{\partial #1}{\partial #2}}
\newcommand \dxdy[2] {\frac{d #1}{d #2}}
\newcommand \fxfy[2] {\frac{\delta #1}{\delta #2}}

\newcommand{\dbf}[1]{d{\mathbf#1}}
\newcommand{\mbf}[1]{{\mathbf#1}}
\newcommand{\mbfr}{{\mathbf r}}
\newcommand \E[1] {Eq.~(\ref{#1})}
\newcommand \Es[1] {Eqs.~(\ref{#1})}
\newcommand \Ess[2] {Eqs.~(\ref{#1}) {\rm and} (\ref{#2})}
\newcommand \EE[1] {Equation~(\ref{#1})}
\newcommand \chil {\chi(0,\mu_i^{eq})}
\newcommand \chis {\chi(1,\mu_i^{eq})}

\newcommand \bsy[1] {\boldsymbol{#1}}


%%%NIKS COMMANDS END%%%%%%%%%%%%


%\allowdisplaybreaks
\begin{document}
\bibliographystyle{unsrt}

\title{Helmholtz Formulation of Phase Field Method for Multi-Phase, Multi-Component Solidification} 

\author{Tatu Pinomaa}
\affiliation{Technical Research Centre of Finland and Aalto University, Espoo, Finland}
\author{Nikolas Provatas}
\affiliation{Physics Department, Centre for the Physics of Materials, McGill University, Canada}

\date{today}

\begin{abstract}
This tutorial goes through the derivation of a phase field theory for solidification of multi-component mixtures and multiple order parameters. The model developed is suitable for the study of multiple orientations or phases during primary, and secondary phase solidification, and for multiple concentration fields. The approach builds on two previous approaches. One is the quantitative multi-order parameter model of Ofori-Opoku et. al (2010) for two-phase solidification and the second is the grad potential formulation of Plapp (2012). The first work generalizes a previous single phase solidification theory for dilute alloys to the case of any two-phase solidification system with multiple phases or orientations.  The second work derives phase field equations of motion from a grand potential functional, which unifies the mathematics of solidification for binary alloy and a pure substance. 
\end{abstract}

\maketitle

\tableofcontents


\section{Helmholtz formulation dynamics}

We start off with the Helmholtz free energy, with an interpolation of the phases according to Kim [Kim SG, Kim WT, Suzuki T. Phys Rev E 1999;60:7186.]: 
\begin{align}
F[\bsy{\phi},\bsy{\mu}] = \int_{V}  \left\{ f_{\text{int}}\lt( \bsy{\phi},\bsy{\nabla}\bsy{\phi}\rt) + \sum_{\alpha=1}^{N}g_{\alpha}(\bsy{\phi}) f^{\alpha}(\bsy{c}) + \left[1 - \sum_{\alpha=1}^{N}g_{\alpha}(\bsy{\phi}) \right] f^{\ell}(\bsy{c}) \right\}\, d^3 \bsy{r}
\label{grandPotFunc}
\end{align}
The interpolator $g_\alpha$ should be zero in liquid, and one in solid; this is satisfied by the following commonly used polynomial:
\begin{align}
g_\alpha(\bsy{\phi}) &= \frac{1}{2} \left[ \frac{15}{8}\left(\frac{1}{5}\phi_\alpha^5 - \frac{2}{3}\phi_\alpha^3 + \phi_\alpha \right) + 1 \right], \nline
g_\alpha'(\bsy{\phi}) &= \frac{1}{2} \cdot\frac{15}{8}\left( \phi_\alpha^4 -2\phi_\alpha^2 + 1 \right) \nline
&=  \frac{1}{2} \cdot\frac{15}{8} \left( 1 - \phi_\alpha^2 \right)^2.
\end{align} 

The variation with respect to phase fields and concentrations:
\begin{align}
\delta F = \int_{V} & \left( \sum_{\alpha}^{N}\lt\{-\sigma^2_{\alpha}\bsy{\nabla}^{2}\phi_{\alpha} + H_{\alpha}f^{\prime}_{\text{DW}}(\bsy{\phi}_{\alpha}) + \sum_{\beta \ne \alpha}\Psi^{\prime}({\phi}_{\alpha},{\phi}_{\beta})+ g^{\prime}_{\alpha}(\bsy{\phi})\lt[f^{\alpha}(\bsy{c}) - f^{\ell}(\bsy{c})\rt] \rt\} \delta\phi_{\alpha} \right. \nline
& +  \left. \sum_i \lt\{\sum_{\alpha}^{N}g_{\alpha}(\bsy{\phi})\pxpy{f^{\alpha}(\bsy{c})}{c_{i}} + \left[1 - \sum_{\alpha}^{N}g_{\alpha}(\bsy{\phi}) \right] \pxpy{f^{\ell}(\bsy{c})}{c_{i}}\rt\}\delta c_{i} \right) d^3 \bsy{r}.
\label{gradPotFuncVar}
\end{align}

Phase field dynamics:
\begin{align}
\pxpy{\phi_{\alpha}}{t} = - \frac{1}{\tau_{\alpha}} \fxfy{F}{\phi_{\alpha}},
\label{evolPhi}
\end{align}

\begin{align}
\tau_\alpha \pxpy{\phi_{\alpha}}{t} & =   \sigma^2_{\alpha}\bsy{\nabla}^{2}\phi_{\alpha} - H_{\alpha}f^{\prime}_{\text{DW}}(\bsy{\phi}_{\alpha}) - \sum_{\beta \ne \alpha}\partial_{\alpha}\Psi({\phi}_{\alpha},{\phi}_{\beta}) -\lt[f^{\alpha}(\bsy{c}) - f^{\ell}(\bsy{c})\rt]  g^{\prime}_{\alpha}(\bsy{\phi}). \nline 
\Leftrightarrow
\pxpy{\phi_{\alpha}}{t}& =  \frac{1}{\tau_\alpha}\left[  \frac{\sigma_\alpha^2}{H_\alpha}\bsy{\nabla}^{2}\phi_{\alpha} -  f^{\prime}_{\text{DW}}(\bsy{\phi}_{\alpha}) - \frac{1}{H_\alpha}  \sum_{\beta \ne \alpha}\partial_{\alpha}\Psi({\phi}_{\alpha},{\phi}_{\beta}) - \frac{1}{H} \lt[f^{\alpha}(\bsy{c}) - f^{\ell}(\bsy{c})\rt]  g^{\prime}_{\alpha}(\bsy{\phi}) \right],
\label{evolPhiFull}
\end{align}
where both nucleation barrier $H$, and the free energy densities $f_\vartheta$ have units of $J/m^3$. 

Concentration dynamics:

\begin{align}
\pxpy{c_{i}}{t} &= \bsy{\nabla} \cdot \left( \sum_{j} M_{ij}(\bsy{\phi}, \bsy{c}, T) \bsy{\nabla}\left(\fxfy{F}{c_j} \right) \right),
\nline
&= \bsy{\nabla} \cdot \left( \sum_{j} M_{ij}(\bsy{\phi}, \bsy{c}, T) \bsy{\nabla}\mu_{j} \right),
\nline
M_{ij}(\bsy{\phi}, \bsy{c}, T)& = \sum_\alpha g_\alpha (\bsy{\phi}) \left( \pxpy{}{c_i}\pxpy{}{c_j}  f_\alpha(\bsy{c},T) \right)^{-1} D_{ij}^\alpha (T) \nline
& + \left[1- \sum_\alpha g_\alpha (\bsy{\phi}) \right] \left( \pxpy{}{c_i}\pxpy{}{c_j}  f_\ell(\bsy{c},T) \right)^{-1} D_{ij}^\ell (T), 
\nline
\mu_j(\bsy{\phi},\bsy{c},T) & = \fxfy{F}{c_j}  = \sum_\alpha g_\alpha (\bsy{\phi}) \pxpy{}{c_j} f^\alpha(\bsy{c}) +  \left( 1- \sum_\alpha g_\alpha (\bsy{\phi}) \right) \pxpy{}{c_j} f^\ell (\bsy{c}),
\label{evolConcFull-2}
\end{align}
where $D_{ij}^\vartheta$ is the interdiffusion coefficient between species $i$ and $j$ in phase $\vartheta \in \alpha, \ell$. We start off by assuming that diffusion is negligible in solids. 

The diffusion of species $i$ can be expressed as 
\begin{align}
\pxpy{c_i}{t} & = \nabla \cdot  \bsy{J}_i
\end{align}

\begin{align}
\bsy{J}_i & = \sum_{j=1}^K M_{ij} \nabla \mu_j(\bsy{c})  \nline
 &=  \sum_{j=1}^K M_{ij} \left[ \sum_{k=1}^{K} \pxpy{\mu_j}{c_k} \nabla c_k \right] \nline 
 &=  \sum_{k=1}^K \sum_{j=1}^{K} M_{ij}  \pxpy{\mu_j}{c_k} \nabla c_k \nline 
 &= \sum_{k=1}^K D_{ik} \nabla c_k,
\end{align}
where we have made the definition 
\begin{align}
D_{ik} = \sum_{j=1}^{K} M_{ij}  \pxpy{\mu_j}{c_k},
\end{align}
and $f=f(\bsy{c},T)$ is the phase-interpolated free energy of the system, and $\pxpy{\mu_j}{c_k} = \pxpy{}{c_j}\pxpy{}{c_k}f$ is the thermodynamic factor. Expressing the solute flux through a concentration gradient is questionable to some extent; for example, in reality solute segregation can occur even if initially the concentration is completely uniformly (no concentration gradients initially).


\section{Phase-field parameters}
\subsection{Estimating the nucleation barrier H}
According to classical nucleation theory, we need only the free energy difference between each phase, $\Delta G$, and the surface tension $\sigma$ to estimate the critical nucleation radius and energy: $r_{crit} = -2 \Delta G/\sigma$, $\Delta G_{crit} = 16 \pi \sigma^3 /(3 \Delta G V_{crit})$. For cobalt, $\sigma \sim 0.2$ J/m$^2$, and $\Delta G \sim 1e10$  J/m$^3$, giving $\Delta G_{crit} \sim 1e9 $ J/m$^3$. 

\subsection{Sharp interface mapping}

The following sharp interface mappings hold true up to second order for a binary alloy, even for the Helmholtz formulation where there is a concentration-coupling in the steady-state phase field and concentration profiles. 
\begin{align}
d_o &= a_1 \frac{W}{\Delta c \lambda}, \nline 
\beta &= a_1  \frac{\tau}{\hat{\lambda} W} \Big(1 - a_2 \hat{\lambda} \frac{W^2}{\tau D} \Big)
\end{align}
where $\lambda$ is the inverse nucleation barrier, with units $m^3/J$, and $\Delta c = (c_l - c_s)R T_m/\Omega$ is the concentration jump across the interface, from one bulk to the other.

The coefficient $a_1$ is a constant when steady-state phase field profile is decoupled from concentration equation. Even in case concentration and phase field steady-state profiles are coupled, the corrections come to the second order terms. Here we neglect the first equation, and estimate $\tau$ by fixing $W$, $\beta$, and nucleation barrier. The kinetic coefficient for the temperature sharp interface equation is related to the  concentration sharp interface kinetic coefficient as $\beta_c = 1/(\Delta T \mu_T)$ [Echebarria 2004], where $\Delta T$ is the freezing range for the alloy. Typically, $\mu_T$ is between 0.02 and 0.2 Kms$^{-1}$



\subsection{Surface energy}
Grand potential formulation leads to decoupling of the solid-liquid and solid-solid equilibrium phase field profiles. Thus the solid-liquid interface energy reduces to a simple integral, which has a constant value. Moreover, the "traditional" sharp interface asymptotics can be employed, to directly tune the surface energy with phase field parameters such as the computational interface width.

In the Helmholtz formulation, the equilibrium phase field profile remains coupled with the concentration equation, except for a special case such as dilute binary alloy (which also has to be made non-variational).  In general, the emergent surface energy depends on the "position" in the phase diagram (temperature, concentration), through boundary conditions for concentration far away from the interfaces. 

Instead of using a phenomenological and simple double well representation for the Landau term,  in reality there could be a concentration dependence, which might be hard to estimate. 


\subsection{W-C-Co thermodynamic system}
We start off with three phases: liquid, tungsten carbide (WC), and solid cobalt (FCC$\_$A1). 

For the thermodynamic assessment, tungsten carbide has no measurable solubility, and its free energy is set to depend only on temperature. However, for the phase field modeling we need to assume a small solubility range (which can be true with accurate-enough measurements). We thus model the tungsten carbide free energy surface as a sharp paraboloid, where the minimum follows the "exact" form from the database. 

Liquid phase has a solubility throughout the composition range. 

Solid cobalt uses a two-sublattice model (Co,W;C,VA): the Co or W can occupy the first sublattice, and the second sublattice is occupied either by C or a vacancy (VA). This means that the maximum molar fraction of carbon can't exceed 50 \%, whereas Co and W can vary from 0 to 100 \%. 

\subsection{Dimensions, non-dimensionalization}

Note that thermodynamic databases yield the free energy per mole (of a phase), not the free energy per volume that we need for our current formulation. Unlike some authors, we will not assume that all the phases have the same molar volume: this would scale the free energies of the phases unrealistically with respect to each other, leading to questionable thermodynamic driving forces. Note that also element convervation equation needs to be modified if molar volume is not constant. 


\section{Diffusion, mobilities and related thermodynamic data}

\subsection{Mechanisms in solid and liquid}
Liquid diffusion is not understood as well as solid diffusion. It is still strongly thermally activated, and an Arrhenius-type dependence of diffusivity $D(T) = D_0 \exp(-Q/RT)$ is sometimes used [Wahlbrühl 2014, KTH thesis]. 

Solid diffusion is either interstitial or substitutional. Substitutional is usually less strong, as it has a larger energy (activation) barrier and it requires a a vacancy. Interstitial is possible typically for smaller atoms.

In contact with air,  there is also surface diffusion, which is typically larger than bulk diffusion. Diffusion is also larger around grain boundaries.

Diffusion can be measured with tracer elements, which give a good estimate of the self-diffusion. 
 
%  \subsection{Assumptions}
%This approach is reasonable when we assume that for
%\begin{itemize}
%\item substitutional diffusion, the molar volume of both species are equal 
%\item interstitial diffusion, the molar volume of the diffusing species is negligible. 
%\end{itemize}
%These assumptions are problematic when the1 molar volume differences can lead to stresses -- even these stresses are expected to relax fast near melting temperature [Engström and Ågren, 1992].

\section{Air phase}
Lets assume that the solidification is surrounded by air, which is an ideal gas. Ideal gas has no surface tension, and therefore the gradient penalt is negligible. The total energy contains only double well, repulsion, and ideal mixing:
\begin{align}
F_{air} = c^2 (1-c)^2 + \frac{1}{2} w_{air} \sum_{\vartheta \in \alpha, \ell}c^2 \phi_{\vartheta} +  c  \ln{c}+ (1-c) \ln{(1-c)}.
\end{align}
Air is a conserved quantity (conservation more ambiguous for vacuum), and it follows Model B dynamics:
\begin{align}
\pxpy{c}{t} & = \nabla \cdot \left[ M_{air} \nabla \left( \fxfy{ F_{air} }{c} \right) \right] \nline
& = \nabla \cdot \left[M_{air} \left( c-c^3 + w_{air} \sum_{\vartheta \in \alpha, \ell}c \phi_{\vartheta} +  \ln{c}  -   \ln{(1-c)} \right) \right] 
\end{align}

\section{Comparing helmholtz and grand potential formulations}
\subsection{c or mu as the dynamical variable}

\section{Evolution with second time derivative} 

\section{Modeling friction}



\bibliography{bibliomaster.bib}
\end{document} 
