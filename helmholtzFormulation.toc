\tocdepth@munge 
\contentsline {section}{\numberline {}Contents}{2}{}
\tocdepth@restore 
\contentsline {section}{\numberline {I}Helmholtz formulation dynamics}{2}{}
\contentsline {section}{\numberline {II}Phase-field parameters}{4}{}
\contentsline {subsection}{\numberline {A}Estimating the nucleation barrier H}{4}{}
\contentsline {subsection}{\numberline {B}Sharp interface mapping}{5}{}
\contentsline {subsection}{\numberline {C}Surface energy}{5}{}
\contentsline {subsection}{\numberline {D}W-C-Co thermodynamic system}{6}{}
\contentsline {subsection}{\numberline {E}Dimensions, non-dimensionalization}{6}{}
\contentsline {section}{\numberline {III}Diffusion, mobilities and related thermodynamic data}{6}{}
\contentsline {subsection}{\numberline {A}Mechanisms in solid and liquid}{6}{}
\contentsline {section}{\numberline {IV}Air phase}{7}{}
\contentsline {section}{\numberline {V}Comparing helmholtz and grand potential formulations}{7}{}
\contentsline {subsection}{\numberline {A}c or mu as the dynamical variable}{7}{}
\contentsline {section}{\numberline {VI}Evolution with second time derivative}{7}{}
\contentsline {section}{\numberline {VII}Modeling friction}{7}{}
\contentsline {section}{\numberline {}References}{7}{}
