\tocdepth@munge 
\contentsline {section}{\numberline {}Contents}{2}{section*.1}
\tocdepth@restore 
\contentsline {section}{\numberline {I}A Brief History of Phase Field Modelling}{2}{section*.2}
\contentsline {section}{\numberline {II}Overview of this Tutorial}{8}{section*.3}
\contentsline {section}{\numberline {III}Grand Potential Thermodynamics}{10}{section*.5}
\contentsline {section}{\numberline {IV}Grand Potential Phase Field Functional}{12}{section*.6}
\contentsline {subsubsection}{\numberline {1}Interaction between order parameters}{13}{section*.7}
\contentsline {subsubsection}{\numberline {2}Properties of the single-phase grand potential $\omega ^{\vartheta }(\boldsymbol {\mu })$}{14}{section*.8}
\contentsline {subsubsection}{\numberline {3}Concentration of a multi-phase system in the grand potential ensemble}{14}{section*.9}
\contentsline {section}{\numberline {V}Phase Field Dynamics $\{\phi _\alpha ,c_i\}$ Vs. $\{\phi _\alpha ,\mu _i\}$}{15}{section*.10}
\contentsline {subsection}{\numberline {A}Time Evolution in the traditional fields $\{\phi _\alpha \}$ and $\{c_i\}$}{16}{section*.11}
\contentsline {subsection}{\numberline {B}Reformulation of phase field model dynamics in terms of $\phi _\alpha $ and $\mu _i$}{16}{section*.12}
\contentsline {subsubsection}{\numberline {1}Order parameter equaitons}{17}{section*.13}
\contentsline {subsubsection}{\numberline {2}Chemical potential equations}{17}{section*.14}
\contentsline {section}{\numberline {VI}Equilibrium Properties of Grand Potential Functional}{18}{section*.15}
\contentsline {subsubsection}{\numberline {1}Equilibrium concentration field}{18}{section*.16}
\contentsline {subsubsection}{\numberline {2}Equilibrium solid-liquid interfaces}{19}{section*.17}
\contentsline {subsubsection}{\numberline {3}Equilibrium solid-solid interfaces}{19}{section*.18}
\contentsline {subsubsection}{\numberline {4}Classical mechanics analogy}{20}{section*.19}
\contentsline {section}{\numberline {VII}Quantitative Grand Potential Phase Field Simulations}{21}{section*.20}
\contentsline {section}{\numberline {VIII}Thermal Fluctuations in the Phase Field Equations}{24}{section*.21}
\contentsline {subsubsection}{\numberline {1}Non-dimensional form of the phase field equations}{24}{section*.22}
\contentsline {subsubsection}{\numberline {2}Simplification of the noise amplitude for the order parameter equation}{25}{section*.23}
\contentsline {subsubsection}{\numberline {3}Simplification of the noise amplitude for the solute equation}{26}{section*.24}
\contentsline {section}{\numberline {IX}Special Cases of the Grand Potential Phase Field Model}{27}{section*.26}
\contentsline {subsection}{\numberline {A}Polycrystalline Multi-Component Alloy Solidification at Low Supersaturation}{27}{section*.27}
\contentsline {subsubsection}{\numberline {1}Evaluating the equilibrium reference chemical potentials $\mu _i^{\rm eq}$ }{29}{section*.28}
\contentsline {subsubsection}{\numberline {2}Re-casting the differential equations in Eqs.\nobreakspace {}(\ref {revised_PF_eqs}) in terms of supersaturation}{30}{section*.29}
\contentsline {subsubsection}{\numberline {3}Special limits I: A multi-component version of the model of Ofori-Opoku et al \cite {Ofori-Opoku10}}{31}{section*.30}
\contentsline {subsubsection}{\numberline {4}Special limits II: recovering the binary alloy Supersaturation Limit of Plapp \cite {Plapp11}}{33}{section*.31}
\contentsline {subsection}{\numberline {B}Polycrystalline Binary Alloy with Quadratic Solid/Liquid Free Energies}{34}{section*.32}
\contentsline {subsubsection}{\numberline {1}Solid-liquid phase coexistence}{35}{section*.33}
\contentsline {subsubsection}{\numberline {2}Grand potential of a phase, and multi-phase concentration and susceptibility}{36}{section*.34}
\contentsline {subsubsection}{\numberline {3}Casting the phase field equations in "supersaturation form"}{36}{section*.35}
\contentsline {subsection}{\numberline {C}Multi-Phase, Multi-Component Alloys with Quadratic Free Energies }{38}{section*.36}
\contentsline {subsubsection}{\numberline {1}Free energy and susceptibility of a single phase}{39}{section*.37}
\contentsline {subsubsection}{\numberline {2}Vector notation and transformations between concentrations and chemical potentials}{39}{section*.38}
\contentsline {subsubsection}{\numberline {3}Grand potential and concentration of a single phase}{40}{section*.39}
\contentsline {subsubsection}{\numberline {4}Multi-phase concentration, susceptibility and concentration difference}{41}{section*.40}
\contentsline {subsubsection}{\numberline {5}Grand potential driving force for multi-phase solidification}{43}{section*.41}
\contentsline {subsubsection}{\numberline {6}Casting the driving force in term of supersaturations}{44}{section*.42}
\contentsline {subsubsection}{\numberline {7}Final form of phase field equations in terms of supersaturation driving forces}{44}{section*.43}
\contentsline {section}{\numberline {X}Asymptotics in the Grand Potential Phase Field Model}{46}{section*.45}
\contentsline {subsection}{\numberline {A}What is ``asymptotic analysis" of a phase field model about? }{47}{section*.46}
\contentsline {subsection}{\numberline {B}Interpreting the role of $\lambda $ as a convergence parameter in solidification }{48}{section*.47}
\contentsline {subsection}{\numberline {C}Interpreting the dual role of $\lambda $ in asymptotics and in the noise amplitide}{49}{section*.48}
\contentsline {section}{\numberline {XI}Continuous Growth Kinetics in the Diffuse Interface Limit of the Grand Potential Phase Field Equations}{51}{section*.50}
\contentsline {subsection}{\numberline {A}Review of the Continuous Growth Model of Rapid Solidification}{52}{section*.51}
\contentsline {subsubsection}{\numberline {1}Kinetic undercooling of the interface in Henrian solutions}{54}{section*.52}
\contentsline {subsection}{\numberline {B}Continuous Growth Model Limit of the Grand potential Phase Field Model}{55}{section*.53}
\contentsline {subsubsection}{\numberline {1}Specializing Eq.\nobreakspace {}(\ref {CGM4}) into the CGM model of Eq.\nobreakspace {}(\ref {CGM6}): full drag case}{57}{section*.54}
\contentsline {subsubsection}{\numberline {2}Specializing Eq.\nobreakspace {}(\ref {CGM4}) into the CGM model with zero drag}{58}{section*.55}
\contentsline {subsubsection}{\numberline {3}Relating $1/v_c^{\rm PF}$ to interface kinetic coefficient $\beta $ for the case of ideal binary alloys }{59}{section*.56}
\contentsline {subsection}{\numberline {C}Non-equilibrium partition coefficient $k(v_0)$}{60}{section*.57}
\contentsline {subsubsection}{\numberline {1}Chemical potential jump at the interface}{60}{section*.58}
\contentsline {subsubsection}{\numberline {2}Evaluating $\Delta \mathaccentV {bar}016{F}$ and an equation for $k(v_o)$ from Eq.\nobreakspace {}(\ref {chem_jump})}{62}{section*.59}
\contentsline {subsubsection}{\numberline {3}Evaluating $k(v_o)$ for ideal binary alloys}{63}{section*.60}
\appendix 
\contentsline {section}{\numberline {A}Incorporating Temperature in the Grand Potential Phase Field Model }{64}{section*.62}
\contentsline {section}{\numberline {B}Asymptotic Analysis of the Grand Potential Phase Field Equations}{67}{section*.63}
\contentsline {subsection}{\numberline {1}Length and Time Scales}{67}{section*.64}
\contentsline {subsection}{\numberline {2}Phase Field Equations written in perturbation variables}{68}{section*.65}
\contentsline {subsubsection}{\numberline {a}Convenient notation and definitions}{69}{section*.66}
\contentsline {subsection}{\numberline {3}Field Expansions and Matching Conditions of Outer/Inner Solutions}{69}{section*.67}
\contentsline {subsection}{\numberline {4}Outer Equations Satisfied by Phase Field Equations}{71}{section*.68}
\contentsline {subsection}{\numberline {5}Inner Equations Satisfied by the phase field equations }{71}{section*.69}
\contentsline {subsubsection}{\numberline {a}Phase field equation}{73}{section*.70}
\contentsline {subsubsection}{\numberline {b}Chemical potential equation}{73}{section*.71}
\contentsline {subsubsection}{\numberline {c}Constitutive relation between $c$ and $\mu $}{74}{section*.72}
\contentsline {subsection}{\numberline {6}Analysis of Inner Equations and Matching to Their Outer Fields }{74}{section*.73}
\contentsline {subsubsection}{\numberline {a}$\mathcal {O}(1)$ phase field equation Eq.\nobreakspace {}(\ref {phi_in_0})}{74}{section*.74}
\contentsline {subsubsection}{\numberline {b}$\mathcal {O}(1)$ diffusion equation\nobreakspace {}(\ref {c_in_0})}{74}{section*.75}
\contentsline {subsubsection}{\numberline {c}$\mathcal {O}(\epsilon )$ phase field equation\nobreakspace {}(\ref {phi_in_1})}{76}{section*.76}
\contentsline {subsubsection}{\numberline {d}$\mathcal {O}(\epsilon )$ diffusion equation\nobreakspace {}(\ref {c_in_1})}{77}{section*.77}
\contentsline {subsubsection}{\numberline {e}$\mathcal {O}(\epsilon ^2)$ phase field equation\nobreakspace {}(\ref {phi_in_2})}{79}{section*.78}
\contentsline {subsubsection}{\numberline {f}$\mathcal {O}(\epsilon ^2)$ diffusion equation\nobreakspace {}(\ref {c_in_2})}{81}{section*.79}
\contentsline {section}{\numberline {}References}{84}{section*.80}
