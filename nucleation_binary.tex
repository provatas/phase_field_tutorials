\documentclass[12pt,letterpaper]{article}

\usepackage{amsmath} % just math
\usepackage{amssymb} % allow blackboard bold (aka N,R,Q sets)
\usepackage{ulem}
\usepackage{graphicx}
%\linespread{1.6}  % double spaces lines
\usepackage[left=1in,top=1in,right=1in,bottom=1in,nohead]{geometry}

\begin{document}

%\linespread{1} % single spaces lines
%\small \normalsize %% dumb, but have to do this for the prev to work

\title{Notes on Nucleation of a Solid Phase From an Undercooled Liquid in a Dilute Binary Alloy: Theory and Application} 

\author{David Montiel}
\maketitle
\linespread{1}

Our main objective is to derive an expression for the nucleation barrier associated with the formation of a solid nucleus within a 
 a metastable liquid in a binary alloy. This barrier, which we denote as $\Delta F^{*}$, is used in the calculation of the nucleation 
 rate, $J_N$, given by

\begin{equation}
\label{eq_nuc_rate}
J=N \beta^{*} Z \exp(-\Delta F^{*}/kT),
\end{equation}   
\\
\noindent where $N$ is the nucleation site density, $\beta^{*}$ is a frequency factor equal to the 
reciprocal of the characteristic nucleation time, $Z$ is the Zeldovich factor and
$k$ is the Boltzmann constant.\\

\section{General free energy}

Consider a large closed thermodynamic system of volume $V$ containing a homogeneous undercooled liquid phase in contact with a thermal reservoir at temperature $T$. Let $f_L(c,T)$ be the composition and temperature-dependent free energy density of this liquid. Figure \ref{PHD1} shows a typical composition dependance of $f_L$ for a binary alloy at a fixed temperature. Notice that in order for the liquid to be in a metastable state, its composition $c_0$ must be smaller than the coexistance liquid composition, $c_L^{eq}$, obtained via the common tangent construction with the solid phase curve. Thus, the initial free energy of the system is
\\
\begin{equation}
\label{FE0}
 F_0=f_L(c_0,T)V.
\end{equation}   
\begin{figure}
\begin{center}
\includegraphics[width=0.75\textwidth]{PHD1.jpg}
\caption{Liquid energy landscape}
\label{PHD1}
\end{center}
\end{figure}
\\
Now consider that in the bulk liquid, a fluctuation gives rise to small spherical solid nucleus of size $V_N$ and composition $c_N$ (see Fig. \ref{nuc_event}). The free energy of the system is now given by
\\
\begin{figure}
\begin{center}
\includegraphics[width=0.9\textwidth]{nuc_event.jpg}
\caption{Nucleation event}
\label{nuc_event}
\end{center}
\end{figure}
\begin{equation}
\label{FE1}
F=f_S(c_N,T)V_N+f_L(c_L^\prime,T)(V-V_N)+\gamma A_N,
\end{equation}   
\\
\noindent where $f_S$ is the extensive free energy of the solid, $\gamma$ is the solid-liquid interfacial energy (here we consider it to be to be orientation independent), $A_N$ is the surface area of the nucleus and $c_L^\prime$ is the composition of the liquid after the nucleus has been formed; it is different from $c_0$ because  $c_N < c_0$ and the average composition for the whole system must remain constant. Since we are assuming that the transformation is isothermal, it is convenient to drop the explicit dependence of temperature in the equations. The associated change in free energy is:\\
\\
\begin{align}
\label{Delta_FE}
\Delta F & =  F-F_0 \nonumber \\
 & =  f_S(c_N)V_N+f_L(c_L^\prime)(V-V_N)+\gamma A_N - f_L(c_0)V \nonumber \\
 & =  [f_S(c_N)- f_L(c_0)]V_N+[f_L(c_L^\prime)-f_L(c_0)V](V-V_N)+\gamma A_N.
\end{align}
\\
\noindent In the equation above it is also implied that the density of the solid nucleus is the same as the liquid. We also have the solute conservation equation, which is a consequence of considering a closed system:
\\
\begin{equation}
\label{sol_cons}
c_0 V = c_N V_N + c_L^\prime (V-V_N) 
\end{equation}   
\\
Since the system is large, $V_N \ll V$ and $c_L^\prime$ is close to $c_0$. Therefore, we can write $f_L(c_L^\prime)$ as an expansion (to first order) of $f_L$ around $c_0$:
\\
\begin{align}
\label{fL_expansion}
f_L(c_L^\prime) & = f_L(c_0)+ \left . \frac{\partial f_L}{\partial c} \right | _{c_0} (c_L^\prime - c_0) \nonumber \\
&  = f_L(c_0)+\mu_L(c_0) (c_L^\prime - c_0),
\end{align}   
\\
\noindent where $\mu_L(c_0)$ (which from now on we denote as $\mu_0$) is the excess chemical potential of the liquid at $c_0$. Substituting $f_L(c_L^\prime)$ 
from \eqref{fL_expansion} and $V-V_N$ from \eqref{sol_cons} into \eqref{Delta_FE} we get
\\
\begin{equation}
\label{Delta_FE2}
\Delta F =  \underbrace{ \left [ f_S(c_N)-f_L(c_0)-\mu_0 (c_N-c_0) \right ]V_N}_{\Delta F_B} +
\underbrace{\gamma A_N}_{\Delta F_I}.
\end{equation}   
\\
Note that we can identify two contributions to the change in free energy: the bulk contribution $\Delta F_B$
and the interface contribution $\Delta F_I$. Since the former is associated with the change from a metastable phase (liquid) into a stable phase (solid),  $\Delta F_B$ is always negative. In contrast, $\Delta F_I$ is always positive because it is associated with the creation of an interface. Now, because of the relation between $V_N$ and $A_N$ (remember we are considering a spherical geometry) the surface term will always dominate for small sizes and the bulk term will start to dominate beyond a certain size. Thus, $\Delta F$ as a function of size will have a maximum which corresponds to the nucleation barrier $\Delta F^{*}$. The critical nucleus size $V_N^{*}$ must satisfy 
\\
\begin{equation}
\label{max_cond}
\left . \frac{\partial \Delta F}{\partial V_N} \right | _{V_N^{*}} = 0
\end{equation}   
\\
But before finding $V_N^{*}$ and $\Delta F^{*}$ we still need to specify the concentration selected by the nucleus, $c_N$. Since nucleation is a fluctuation driven event and thermodynamics tells us that the fluctuation most likely to occur is the one associated with the smallest cost in energy, we can determine $c_N$ by finding the minimum of $\Delta F$ with respect to the concentration  in the solid. Since the dependence of $\Delta F$ on $c_N$ is only on the bulk part, minimizing $\Delta F$ is equivalent to minimizing $\Delta f_B=\Delta F_B/V_N$ or, alternatively, maximizing $D_F \equiv -\Delta f_B$ which we can express as
\\
\begin{equation}
\label{maxdf_cond}
\frac{\partial D_F}{\partial c_N} = 0,
\end{equation}   
\\
\noindent where $D_F$ is the called the thermodynamic driving force for nucleation and it is proportional to the undercooling of the original liquid phase. Obtaining $D_F$ from \eqref{Delta_FE2} and solving \eqref{maxdf_cond} give us
\\
\begin{equation}
\label{mu_S_eq_mu_0}
\frac{\partial f_S}{\partial c_N} = \mu_S (c_N) = \mu_0,
\end{equation}   
\\
\noindent where $\mu_S (c_N)$, which from now on we denote as $\mu_N$ is the solid excess chemical potential at concentration $c_N$. Eq. \ref{mu_S_eq_mu_0} states that the selected nucleus concentration $c_N$ is such that the chemical potential of the solid and liquid phases is the same. We can write now the driving force for nucleation as 
\\
\begin{align}
\label{nucDF}
D_F & = [f_L(c_0)-\mu_0 c_0] -  [f_S(c_N)-\mu_0 c_N] \nonumber \\
 & = -(\omega_S(c_N) - \omega_L(c_0))  \nonumber \\
 & = -\Delta \omega_B,
\end{align}   
\\
\noindent where $\omega$ is the grand 
potential density\footnote{At this point it is important to clarify the distinction between the physical meaning of $\Delta f_B$ and $\Delta \omega_B$ to avoid confusion. $\Delta f_B$ refers to the total change in bulk free energy {\bf of the entire system} associated with the creation of the nucleus divided by the nucleation volume, whereas $\Delta \omega_B$ refers to the difference of the igrand potential density (grand potential per unit volume) between the solid and that of the original liquid phase.}. Graphically, the driving force for nucleation is 
represented in figure \ref{DF_plot}. Note that the condition that $\mu_N=\mu_0$ means that the slopes of the lines
tangent to $f_S(c_N)$ and $f_L(c_0)$ are the same. However, these lines have different intercepts. \\
\begin{figure}
\begin{center}
\includegraphics[width=0.9\textwidth]{DF_plot.jpg}
\caption{Graphical representation of driving force for nucleation.}
\label{DF_plot}
\end{center}
\end{figure}
We now write equation \eqref{Delta_FE2} as
\\
\begin{equation}
\label{Delta_FE3}
\Delta F = \Delta \omega_B V_N+\gamma A_N.
\end{equation}   
\\
Finally, finding the maximum of $\Delta F$ with respect to $V_N$ (according to \eqref{max_cond}) gives us the critical nucleus size which, in terms of its radius $R^{*}$, is given by
\\
\begin{equation}
\label{R_crit}
R^{*} = -\frac{2\gamma}{\Delta \omega_B},
\end{equation}   
\\
and the free energy barrier $\Delta F^{*}$,
\\
\begin{equation}
\label{FE_barrier1}
\Delta F^{*} =\frac{16 \pi \gamma^3}{3 \Delta \omega_B^2}.
\end{equation}   
\\
\section{Dilute limit}
The free energy for a dilute binary alloy can be written as an expansion around the free energy of the pure material at $T_m$\footnote{Echebarria et al., PRE {\bf 70}, 061604 (2004).} 
\\
\begin{equation}
\label{Dilute_FE}
f_\nu(c,T) =f^A(Tm)-s_\nu(T-T_m)+ \frac{RT_m}{v_0}(c\ln c-c)+ \varepsilon_\nu c,
\end{equation}   
\\
\noindent where $f^A$ is the free energy density of the pure material and $T_m$ its corresponding melting point. Similarly $s$ and $\varepsilon$ are the respective entropy and internal energy densities of the pure material at $T_m$. $v_0$ is the molar volume and R is the gas constant. The subindex, $\nu=S/L$ indicates the phase (solid or liquid). The excess chemical potential is given by
\\
\begin{equation}
\label{Dilute_mu}
\mu_\nu =\frac{RT_m}{v_0}\ln c + \varepsilon_\nu.
\end{equation}   
\\
The condition of equality of the chemical potential of each phase ($\mu_N=\mu_0$) gives us the relation between $c_N$ and $c_0$:
\\
\begin{align}
\label{Dilute_mu_eq}.
\frac{RT_m}{v_0}\ln c_N + \varepsilon_S & = \frac{RT_m}{v_0}\ln c_0 + \varepsilon_L  \\
\frac{c_N}{c_0} & =\exp \left ( -\frac{v_0\Delta \varepsilon}{R T_m}\right ) = k,
\end{align}   
\\
\noindent where $\Delta \varepsilon = \varepsilon_S - \varepsilon_L$ and $k$ is the partition coefficient. Notice that, in the dilute limit,  the condition of equality of excess chemical potentials implies that the concentrations $c_N$ and $c_0$ are related through the {\bf equilibrium} partition coefficient, even though they are non-equilibrium concentrations. \\

We now obtain $\Delta \omega_B$ using \eqref{Dilute_FE} and \eqref{Dilute_mu}
\\
\begin{align}
\label{Dilute_Delta_om}
\Delta \omega_B & =\left [ f_S(c_N)-\mu c_N \right ] - \left [ f_L(c_0)-\mu c_0 \right ] \nonumber \\
 & =-\Delta s (T-T_m) - \frac{RT_m}{v_0}(c_N-c_0) \nonumber \\
 & =\frac{L}{T_m} (T-T_m) - \frac{RT_m}{v_0}(c_N-c_0),
\end{align}   
\\
\noindent where $\mu=\mu_0=\mu_N$  and $\Delta s = s_S-s_L$ is the solidification entropy density change for the pure material, which can be written in terms of the latent heat $L$ and the melting point: $\Delta s=-L/T_m$. We can further simplify \eqref{Dilute_Delta_om} by writing it in terms of the phase diagram parameters $k$ and $m$ (the liquidus line slope) using the van't Hoff relation for binary alloys:
\\
\begin{equation}
\label{vant_Hoff}
m=-\frac{T_m^2 R (1-k)}{v_0 L}
\end{equation}   
\\
Substitution of $L/T_m$ in \eqref{vant_Hoff} into \eqref{Dilute_Delta_om} gives us
\\
\begin{equation}
\label{Dilute_Delta_om2}
\Delta \omega_B = \frac{R T_m}{v_0}(1-k) \left [ c_0-\frac{T-T_m}{m} \right ].
\end{equation}   
\\
This can be further simplified into
\\
\begin{equation}
\label{Dilute_Delta_om3}
\Delta \omega_B  =\frac{R T_m}{v_0}(1-k) \left ( c_0-c_L^{eq} \right ),
\end{equation}   
\\
\noindent which is our final result and can be substituted directly into \eqref{FE_barrier1} to obtain $\Delta F^{*}$. 

\section{Notes on the implementation of grain nucleation on phase field 
simulations}


Our main objective is to incorporate a mechanism for nucleation that couples self-consistently with phase-field dynamics. This mechanism
consists of a series of steps, some of which may vary depending on whether we consider homogeneous or heterogeneous nucleation. \\ 

For heterogeneous nucleation, the model considers that only dispersed particles in the liquid can act as a substrate. In the following, we describe the mechanism for nucleation of equiaxed grains after either a quench or continuous cooling of a binary alloy melt as it currently implemented in the adaptive mesh code in 2D. The temperature profile is assumed to be uniform in space and the latent heat release is neglected. 
For homogeneous nucleation, a nucleation barrier $\Delta F^*$ that depends only on temperature $T$ and local concentration $C$ can be calculated at each time step and at each element of the mesh. These two quantities define the local undercooling. For heterogeneous nucleation, the barrier contains a pre-factor which in theory may be obtained from the contact angle between the solid phase and the inoculant particles. \\

We can broadly describe the nucleation algorithm in the following steps:

\section{Homogeneous Nucleation}
\begin{enumerate}
\item Before the phase-field simulation begins, an appropriately chosen nucleation trial time step $\Delta t_n$ is chosen. This time step must be an integer multiple of the phase field time step $\Delta t_\phi$.  The criteria for choosing  $\Delta t_n$ is described later.
\item The phase-field simulation starts from either an all-liquid domain or a solid domain (put in "by hand") within a liquid.  
\item At each $\Delta t_n$ a nucleation probability is calculated for each liquid element of volume $\Delta V$ \footnote{$\Delta V$ is strictly a 3D volume, so for a 2D simulation we must multiply the element area by a thickness factor that must be chosen to accurately represent the 2D projection of a distribution of nuclei in a 3D domain.}, given by

\begin{equation}
\label{eq_nuc_prob}
P=1-\exp (-J \Delta V \Delta t_n),
\end{equation}

\noindent where J is the nucleation rate, given by 

\begin{equation}
\label{eq_nuc_rate}
J=N \beta^{*} Z \exp(-\Delta F^{*}/kT),
\end{equation}

\noindent where $N$ is the nucleation site density, $\beta^{*}$ is a frequency factor equal to the 
reciprocal of the characteristic nucleation time, $Z$ is the Zeldovich factor and
$k$ is the Boltzmann constant. For a given set of conditions (solute concentration and temperature) for the metastable liquid phase
we calculate the value of $\Delta F^{*}$ from classical nucleation theory using bulk and surface energies:

\begin{equation}
\label{eq_e_barrier}
\Delta F^{*}=\frac{16 \pi}{3} \frac{\gamma ^3}{\Delta f_v^2},
\end{equation}

\noindent where $\gamma$ is the solid-liquid interfacial energy and  $\Delta f_v$ is the bulk free energy change per unit volume, given by

\begin{equation}
\label{eq_bulk_df}
\Delta f_v=f_S(c_N)-f_L(c_0)-\left . \frac{\partial f_L}{\partial c} \right |_{c_0}(c_N-c_0)
\end{equation}

\noindent for a binary alloy. In \eqref{eq_bulk_df}, $c_0$ is the local concentration of the metastable liquid, $c_N=kc_0$ is the concentration of the solid nucleus ($k$ is the partition coefficient in the dilute limit) and $f_S(T,c)$ and $f_L(T,c)$ are the bulk free energy curves for the solid and liquid, respectively.
 
While equations \eqref{eq_nuc_prob} and \eqref{eq_nuc_rate} are, in principle, valid only under uniform and 
constant conditions, we can apply them locally to volume elements and time intervals small enough that conditions do not vary strongly within them. In addition \eqref{eq_nuc_prob} is only valid whenever $P \ll 1$. Thus the time interval $\Delta t_n$ must chosen in such way that nucleation is a relatively rare event.

\item  As the phase field simulation progresses, at every given nucleation trial time, the calculated value of $P$ for each element of the system is compared with a random number ($r$) between $0$ and $1$. A nucleation event is then triggered whenever $r<P$. When this happens, a circular grain of size sufficiently larger than the critical radius $R^{*}$ (obtained from $\Delta F^{*}$) and random orientation is seeded at the center of the corresponding element. The growth of each grain is subsequently governed by solute diffusion under the local non-equilibrium conditions.

\end{enumerate}



\end{document}




